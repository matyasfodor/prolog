reacts(vinegar, salt, 25).
reacts(salt, water, 3).
reacts('brown soap', water, 10).
reacts('pili pili', milk, 7).
reacts(tonic, bailey, 8).

reacts_wrapper(X, Y, Strength):-
  reacts(X, Y, Strength).
reacts_wrapper(X, Y, Strength):-
  reacts(Y, X, Strength).
reacts_wrapper(_, _, 0).

%% compound_strength([], 0).
%% compound_strength([_], 0).
%% compound_strength([H1, H2 | T], Strength):-
%%   compound_strength([H1 | T], FirstSum),
%%   compound_strength([H2 | T], SecondSum),
%%   reacts_wrapper(H1, H2, PairStrength),
%%   Strength is PairStrength + FirstSum + SecondSum.

list_strength(_, [], 0).
list_strength(M, [H1 | T], Sum):-
  reacts_wrapper(M, H1, Strength),
  list_strength(M, T, SublistStrength),
  Sum = Strength + SublistStrength.

compound_strength([], 0).
compound_strength([_], 0).
compound_strength([H1, H2 | T], Strength):-
  compound_strength([H2 | T], SublistStrength),
  list_strength(H1, [H2 | T], FirstSum),
  Strength is FirstSum + SublistStrength.

bin(0, 5, 'no irritation').
bin(5, 12, 'minor irritation').
bin(12, 20, 'minor burning wounds').
bin(20, 30, 'severe burning wounds').
bin(30, inf, 'lethal').

advice(L):-
  compound_strength(L, Strength),
  bin(Lower, Upper, Msg),
  Strength >= Lower,
  Strength < Upper,
  write(Msg).
