use_module(library(lists)).

%% Cubes

%% Possible cover sides (without bottom and top)
cover([1, 3, 5, 6]).
cover([1, 2, 5, 4]).
cover([2, 3, 4, 6]).

%% Shifts

simple_shift([H | Tail], L):-
  append(Tail, [H], Concatenated),
  L = Concatenated.

shifts(L, L, _).
shifts(L, Result, N):-
  NewN is N - 1,
  NewN > 0,
  simple_shift(L, Shifted),
  shifts(Shifted, Result, NewN).

shifts(L, Shifted):-
  length(L, Length),
  shifts(L, Shifted, Length).

%% Reverse
reverse_or_not(L, L).
reverse_or_not(L, Result):-
  reverse(L, Result).

%% Generate all possible covers so far.

possbile_covers(Cover):-
  cover(OriginalCover),
  reverse_or_not(OriginalCover, Rev),
  shifts(Rev, Cover).

%% One special one for the first one.
possbile_covers_noshift(Cover):-
  cover(OriginalCover),
  reverse_or_not(OriginalCover, Rev),
  shifts(Rev, Cover, 1).

%% Cube definitions
cube(1, [t, s, t, r, t, c]).
cube(2, [t, r, c, s, r, s]).
cube(3, [s, c, s, r, c, t]).
cube(4, [r, s, t, r, c, c]).

%% Validators
valid_row([], []).
valid_row([H1 | T1], [H2 | T2]):-
  not(H1 = H2),
  valid_row(T1, T2).

valid(_, []).
valid(L, [H | T]):-
  valid_row(L, H),
  valid(L, T).

%% Map index to shape
%% Takes a list of shapes (1-6 according to the drawing)
%% takes a list of indices and returns the corresponding values in Values
map_index_to_shape(_, [], []).
map_index_to_shape(L, [Index | RestIndices], [V | Vs]):-
  nth1(Index, L, V),
  map_index_to_shape(L, RestIndices, Vs).

%% Helper for 3 cubes
gen_3_cubes([Cube1CoverIndex, Cube1Cover, Cube2CoverIndex, Cube2Cover, Cube3CoverIndex, Cube3Cover]):-
  cube(1, Cube1),
  possbile_covers_noshift(Cube1CoverIndex),
  map_index_to_shape(Cube1, Cube1CoverIndex, Cube1Cover),
  cube(2, Cube2),
  possbile_covers(Cube2CoverIndex),
  map_index_to_shape(Cube2, Cube2CoverIndex, Cube2Cover),
  valid(Cube2Cover, [Cube1Cover]),
  cube(3, Cube3),
  possbile_covers(Cube3CoverIndex),
  map_index_to_shape(Cube3, Cube3CoverIndex, Cube3Cover),
  valid(Cube3Cover, [Cube2Cover, Cube1Cover]).

%% Helper for 4 cubes
gen_4_cubes([Cube1CoverIndex, Cube1Cover, Cube2CoverIndex, Cube2Cover, Cube3CoverIndex, Cube3Cover, Cube4CoverIndex, Cube4Cover]):-
  cube(1, Cube1),
  possbile_covers_noshift(Cube1CoverIndex),
  map_index_to_shape(Cube1, Cube1CoverIndex, Cube1Cover),
  cube(2, Cube2),
  possbile_covers(Cube2CoverIndex),
  map_index_to_shape(Cube2, Cube2CoverIndex, Cube2Cover),
  valid(Cube2Cover, [Cube1Cover]),
  cube(3, Cube3),
  possbile_covers(Cube3CoverIndex),
  map_index_to_shape(Cube3, Cube3CoverIndex, Cube3Cover),
  valid(Cube3Cover, [Cube2Cover, Cube1Cover]),
  cube(4, Cube4),
  possbile_covers(Cube4CoverIndex),
  map_index_to_shape(Cube4, Cube4CoverIndex, Cube4Cover),
  valid(Cube4Cover, [Cube3Cover, Cube2Cover, Cube1Cover]).


%% The actual solution
solution([CoverIndex], [Cover], 1):-
  cube(1, Cube),
  possbile_covers_noshift(CoverIndex),
  map_index_to_shape(Cube, CoverIndex, Cover).
solution([CoverIndex | RestCoverIndex], [Cover | RestCover], N):-
  N > 1,
  NewN is N - 1,
  solution(RestCoverIndex, RestCover, NewN),
  cube(N, Cube),
  possbile_covers(CoverIndex),
  map_index_to_shape(Cube, CoverIndex, Cover),
  valid(Cover, RestCover).
solution(CoverIndex, Cover):-
  solution(CoverIndex, Cover, 4).  

%% Important!!!
%% Here I used the base case to generate multiple solutions.
%% When it got exhausted, it returned false, and the general case 
%% Turned into an infinite loop.
