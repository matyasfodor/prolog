simplify(Exp, Simp):-
  simplify(Exp, 0, Simp).

simplify(A, Acc, Result):-
  integer(A),
  Result is Acc + A.

simplify(A, Acc, A+Acc):-
  atom(A).

simplify(A+B, Acc, Simp):-
  write('s1 '),write(A),write('  b:'),write(B),nl,
  integer(B),
  NewAcc is B + Acc,
  simplify(A, NewAcc, Simp).

simplify(A+B, Acc, Simp + B):-
  write('s2 '),write(A),write('  b:'),write(B),nl,
  atom(B),
  simplify(A, Acc, Simp).
