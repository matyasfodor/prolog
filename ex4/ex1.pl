%% Exercise 1
first([H|_], H).

last([H], H).
last([_ | T], S):-
  last(T, S).

removeOne(T, [T| Tail], Tail).
removeOne(T, [H1| T1], NewList):-
  removeOne(T, T1, T2),
  NewList = [H1 | T2].

removeAll(_, [], []).
removeAll(T, [T| Tail], NewList):-
  removeAll(T, Tail, NewList).
removeAll(T, [H| Tail], [H| T2]):-
  removeAll(T, Tail, T2).

removeSmaller(_, [], []).
removeSmaller(T, [H| Tail], NewList):-
  removeSmaller(T, Tail, T2),
  (H =< T ->
    NewList = T2;
    NewList = [H| T2]
  ).

switchFirst2([H1, H2| T], [H2, H1| T]).

switch_every_two([], []).
switch_every_two([H], [H]).
switch_every_two([H1, H2| T1], [H2, H1| T2]):-
  switch_every_two(T1, T2).

%% Exercise 2

switch_unsorted([H1, H2| T], NewList):-
  H1 > H2 ->
    NewList = [H2, H1| T];
    switch_unsorted([H2| T], T2),
    NewList = [H1| T2].

%% This is meta programming - look it up!
%% times(0, L, L, _):-
%% times(N, L1, L2, F):-
%%   F(L1, L3),
%%   N2 is 
%%   times(N-1, L3, L2).

%% times(N, L, L):-
%%   N is 0.
%% times(N, L1, L2):-
%%   switch_unsorted(L1, L3),
%%   times(N - 1, L3, L2).

ten_times(L1, Result):-
  switch_unsorted(L1, L2),
  switch_unsorted(L2, L3),
  switch_unsorted(L3, L4),
  switch_unsorted(L4, L5),
  switch_unsorted(L5, L6),
  switch_unsorted(L6, L7),
  switch_unsorted(L7, L8),
  switch_unsorted(L8, L9),
  switch_unsorted(L9, L10),
  switch_unsorted(L10, Result).

times(0, L, L).
times(N, L1, Result):-
  switch_unsorted(L1, L2),
  times(N - 1, L2, Result).

is_sorted([]).
is_sorted([_]).
is_sorted([H1, H2 | T]):-
  H1 =< H2,
  is_sorted([H2| T]).


mysort(L, NewList):-
  not(is_sorted(L)) ->
    switch_unsorted(L, L2),
    mysort(L2, NewList);
    NewList = L.

%% Exercise 3 nested list
myflatten([], []).
myflatten([[]| Rest], NewList):-
  myflatten(Rest, NewList).
myflatten([[H|T]| Rest], NewList):-
  myflatten([T| Rest], Flattened),
  NewList = [H| Flattened].

deepflatten([], []).
deepflatten([[]], []).
deepflatten([[H| T1]| T], Result):-
  deepflatten([H| T1], FlattenedHead),
  deepflatten(T, FlattenedBody),
  append(FlattenedHead, FlattenedBody, Result).
deepflatten([H| T], A):-
  deepflatten(T, L2),
  A = [H| L2].

%% Exercise 4 transpose
firstElements([], [], []).
firstElements([[H | T]| RestRows], [H| RestHeads], [T | RestTails]):-
  firstElements(RestRows, RestHeads, RestTails).

transpose([[]| _], []).
transpose(Matrix, Result):-
  firstElements(Matrix, Heads, Tails),
  transpose(Tails, TransposedTails),
  Result = [Heads| TransposedTails].

%% transpose([[A]], [[A]]).
%% transpose([[FirstElement| FirstRowRestElements] | RestRows], Result):-
%%   firstElements(RestRows, FirstColumn, SmallMatrix),
%%   transpose(SmallMatrix, TransposedSmallMatrix),

