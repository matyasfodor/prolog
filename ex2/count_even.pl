is_even(D):-
  A is mod(D, 2),
  A = 0.

split_number(Number, Digit, NewNumber):-
  Digit is mod(Number, 10),
  NewNumber is Number // 10.

count_even(Number, Count):-
  Number < 10,
  (is_even(Number) -> Count is 1 ; Count is 0).

count_even(Number, Count):-
  Number >= 10,
  split_number(Number, Digit, NewNumber),
  count_even(NewNumber, SubCount),
  (is_even(Digit) -> Count is 1 + SubCount ; Count is SubCount).
