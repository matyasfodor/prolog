is_even(D):-
  A is mod(D, 2),
  A = 0.

split_number(Number, Digit, NewNumber):-
  Digit is mod(Number, 10),
  NewNumber is Number // 10.

all_even(Number):-
  Number < 10,
  is_even(Number).

all_even(Number):-
  Number >= 10,
  split_number(Number, D, NewNumber),
  is_even(D),
  all_even(NewNumber).




4246
424