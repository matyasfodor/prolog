%% a,
%% First we have to come up with a state representation.
%% The most intuitive one (the one that's used also in part d)
%% Is a simple array of 4 * 2 = 8 elements.

%% b,
%% We could make this generic, but based on the solution for sudoku
%% It's easier to just hard code the checks that need to be performed
%% Let's follow a top-down approach: If these 4 statements hold
%% we found a solution.
is_solution(State):-
  sum_index(State, [1, 2, 3], 3),
  sum_index(State, [3, 4, 5], 3),
  sum_index(State, [5, 6, 7], 3),
  sum_index(State, [1, 7, 8], 3).

%% sum_index(state, indices to select, sum)
%% We can assume, that the indices are in an ascending order 
%% to make our lives easier (check the last line of the previous statement)

%% sum_index\3 is the public interface meant to be used
%% but we need another intermediate term in order to use aggregation, let's expand the arity!
%% sum_index(State, Indices, CurrentIndex, Aggregator, Sum).
sum_index(State, Indices, Sum):-
  sum_index(State, Indices, 1, 0, Sum).

%% Start easy, define the base case:
%% - Important thing is that there are no indices left - empty array
%% - We simply copy the aggregator to the sum
%% - We don't care about the rest of the state and the current index
sum_index(_, [], _, Aggregator, Aggregator):-!.

%% Let's consider the case when we want to add the current element to the aggregator
%% Things to consider:
%% - We have to add the current element (H) to the aggregator
%% - Recursion should happen in the last step in order to use tail recursion
%% - Current index should be passed forwrd incremented
%% important! this is not a search, there's only one answer, so we need to do a cut
%% to avoid backtracking and go to the next state
sum_index([H| T], [CurrentIndex| TailIndex], CurrentIndex, Aggregator, Sum):-
  !,
  NewAggregator is Aggregator + H,
  NewCurrentIndex is CurrentIndex + 1,
  sum_index(T, TailIndex, NewCurrentIndex, NewAggregator, Sum).

%% Case when CurrentIndex doesn't match the first element of indices
%% Same as above, except for the aggregation.
%% Use _ for unused terms.
sum_index([_| T], Indices, CurrentIndex, Aggregator, Sum):-
  NewCurrentIndex is CurrentIndex + 1,
  sum_index(T, Indices, NewCurrentIndex, Aggregator, Sum).

%% c
%% The simpliest variant is the current layout itself.
variants(A, A).
%% In the chosen representation turning means a shift of 2.
%% [a b c d e f g h] - [c d e f g h a b], [e f g h a b c d], [g h a b c d e f]
%% We only want 3 shifts, as the 4th one is the same.
variants(A, B):-
  shifts(A, B).

%% We should also consider the reverse.
variants(A, B):-
  reverse(A, RevA),
  shifts(RevA, B).

shifts(L, R):-
  shift(L, 2, R).
shifts(L, R):-
  shift(L, 4, R).
shifts(L, R):-
  shift(L, 6, R).

shift(L, 0, L).
shift([H|T], N, R):-
  N > 0,
  append(T, [H], L),
  NewN is N - 1,
  shift(L, NewN, R).

%% d,
%% This is a search problem, so we can use the search problem cookie cutter

%% search(CurrentState,AccTrace,Trace):-
%%         is_solution(CurrentState),
%%         Trace=AccTrace.

%% search(CurrentState,AccTrace,Trace):-
%%         try_action(CurrentState,NewState),
%%         validate_state(NewState),
%%         no_loop(NewState,AccTrace),
%%         search(NewState,[NewState|AccTrace],Trace).

%% The trace will contain all the previous solutions.
%% This is not a typical search problem, no need for separate cases
search():-
  search([]).

search(PrevResults):-
  generate(Result),
  is_solution(Result),
  not_variant(PrevResults, Result),
  write(Result), nl,
  search([Result| PrevResults]).

%% Not sure about the 28 different types of dominoes,
%% I think there are 2 * 7^2 different dominoes
%% Is there a constraint about their reuability?
generate(Result):-
  generate([], 8, Result).

generate(Result, 0, Result):-!.
generate(PrevResult, N, Result):-
  NewN is N - 1,
  between(0, 6, Number),
  generate([Number| PrevResult], NewN, Result).

not_variant([], _).
not_variant([H|T], R):-
  \+variants(H, R),
  not_variant(T, R).
