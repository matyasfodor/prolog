gen(0).
gen(1).
gen(2).
gen(3).
gen(4).
gen(5).
gen(6).
gen(7).
gen(8).
gen(9).

triplet([X, Y, Z]):-
  gen(X),
  gen(Y),
  gen(Z),
  check(X, Y, Y).

check(X, Y, Z):-
  Internal is 10 + X + Y,
  Value is Internal * Internal,
  Internal2 is 10 + Y + Z,
  Value is Internal2 * X.
