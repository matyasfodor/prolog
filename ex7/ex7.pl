p(Graph,Node) :- member(Parent/Node,Graph), write(Parent), nl, fail.
p(_Graph,_Node) :- write('end').


parent_list(Graph, Node, ParentList):-
  findall(Parent, member(Parent/Node, Graph), ParentList).

arc(From,To,Graph) :-
  member(From/To,Graph).

find_path_acc_2(X,X,Path,Path,_).
find_path_acc_2(X,Y,Path,Acc,G) :-
  arc(Z,Y,G), 
        not(member(Z,Acc)), % <------ only difference
  find_path_acc_2(X,Z,Path,[Z|Acc],G).
find_path_2(X,Y,Path,Graph) :-
        find_path_acc_2(X,Y,Path,[Y],Graph).

% This is not really nice. It's a custom function to return the shortest list from a list of lists
shortest_from_list_of_lists([Element], Element).
shortest_from_list_of_lists([First, Second | Rest], Shortest):-
  length(First, FirstLength),
  length(Second, SecondLength),
  (FirstLength < SecondLength -> 
    shortest_from_list_of_lists([First | Rest], Shortest);
    shortest_from_list_of_lists([Second | Rest], Shortest)).

shortest_path(X,Y,Graph, ShortestPath):-
  findall(Path, find_path_2(X, Y, Path, Graph), Paths),
  shortest_from_list_of_lists(Paths, ShortestPath).

reacts(vinegar, salt, 25).
reacts(salt, water, 3).
reacts('brown soap', water, 10).
reacts('pili pili', milk, 7).
reacts(tonic, bailey, 8).

reacts_wrapper(X, Y, Strength):-
  reacts(X, Y, Strength).
reacts_wrapper(X, Y, Strength):-
  reacts(Y, X, Strength).

% advice([], 0).
% advice([First | Rest], Number):-
%   findall(Strength, (reacts_wrapper(First, Other, Strength), member(Other, Rest)), Strengths),
%   sum_list(Strengths, PartialSum),
%   advice(Rest, RestSum),
%   Number is PartialSum + RestSum.

advice(List, Number):-
  advice(List, 0, Number).
advice([], Number, Number).
advice([First | Rest], PrevSum, Result):-
  findall(Strength, (reacts_wrapper(First, Other, Strength), member(Other, Rest)), Strengths),
  sum_list(Strengths, PartialSum),
  Number is PrevSum + PartialSum,
  advice(Rest, Number, Result).

wine_dist_manhattan([], [], Dist, Dist).
wine_dist_manhattan([F1| R1], [F2| R2], Dist, Res):-
  PartialSum is Dist + abs(F1 - F2),
  wine_dist_manhattan(R1, R2, PartialSum, Res).

wine_dist_eucledian([], [], Dist, Res):-
  Res is sqrt(Dist).
wine_dist_eucledian([F1| R1], [F2| R2], Dist, Res):-
  PartialSum is Dist + (F1 - F2) * (F1 - F2),
  wine_dist_eucledian(R1, R2, PartialSum, Res).

wine_dist(Feat1, Feat2, Dist):-
  wine_dist_eucledian(Feat1, Feat2, 0, Dist).


take(0, _, []).
take(N, [H | T], Res):-
  NextN is N - 1,
  take(NextN, T, SubT),
  Res = [H | SubT].


add_to_hist([], Element, [[Element, 1]]).
add_to_hist([[Element, N] | T], Element, Res):-
  NewN is N + 1,
  !,
  Res = [[Element, NewN] | T].
add_to_hist([H| T], Element, [H| NewTail]):-
  add_to_hist(T, Element, NewTail).

build_histogram(List, Hist):-
  build_histogram(List, [], Hist).
build_histogram([], Hist, Hist).
build_histogram([H | T], Hist, Result):-
  add_to_hist(Hist, H, NewHist),
  build_histogram(T, NewHist, Result).

most_frequent(List, MostFrequent):-
  build_histogram(List, Histogram),
  sort(2, @>=, Histogram, [[MostFrequent| _]| _]).

rank_to_list([], []).
rank_to_list([[Farmer |_]| T], [Farmer| Farmers]):-
  rank_to_list(T, Farmers).


% This will be a dumb implementation
% Will sort the whole list instead of keeping the first 5 then inserting any new ones -> popping out the rest
prediction(Bottle, MostFrequent):-
  testdata(Bottle, TestData),
  findall([Farmer, Distance], (traindata(Data, Farmer), wine_dist(TestData, Data, Distance)), FarmersWithDistance),
  sort(2, @=<, FarmersWithDistance, SortedFarmersWithDistance),
  take(5, SortedFarmersWithDistance, FirstFiveWithDistance),
  rank_to_list(FirstFiveWithDistance, FirstFive),
  most_frequent(FirstFive, MostFrequent).

correct_ones(List, N):-
  correct_ones(List, 0, N).
correct_ones([], N, N).
correct_ones([H| T], N, Res):-
  prediction(H, Prediction),
  (testdata_class(H, Prediction) -> 
    NewN is N + 1;
    NewN is N),
  correct_ones(T, NewN, Res).

accuracy():-
  findall(B, testdata(B, _), TestData),
  correct_ones(TestData, CorrectCount),
  length(TestData, Length),
  Accuracy is CorrectCount / Length,
  write(Accuracy).
