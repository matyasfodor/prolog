%% List length
listlength([], 0).
listlength([H | T], Count):-
  listlength(T, PartialCount),
  Count is PartialCount + 1.

%% Average

sum_and_length([], 0, 0).
sum_and_length([H | T], Sum, Length):-
  sum_and_length(T, PS, PL),
  Sum is PS + H,
  Length is PL + 1.
avg(List, Avg):-
  sum_and_length(List, Sum, Length),
  Avg is Sum / Length.


%% Countdisk
countdisk([], 0).
countdisk([w|List], Count):-
  countdisk(List, Pc),
  Count is Pc + 1.
countdisk([b|List], Count):-
  countdisk(List, Pc),
  Count is Pc + 1.
countdisk([n|List], Count):-
  countdisk(List, Count).

%% LOA rounds 1. count
rounds_inner([], 0, 0).
rounds_inner([_| T], State, Count):-
  rounds_inner(T, Ps, Pc),
  State is mod(Ps + 1, 2),
  Count is State + Pc.

rounds(L, Count):-
  rounds_inner(L, _, Count).


%% LOA rounds 2. split
split([], [], []).
split([E], [E], []).
split([H1, H2 | T], S1, S2):-
  split(T, PS1, PS2),
  S1 = [H1 | PS1],
  S2 = [H2 | PS2].

%% LOA rounds 3. merge

merge_i([], [], []).
merge_i([E], [], [E]).
merge_i([H1| T1], [H2| T2], List):-
  merge_i(T1, T2, Pl),
  List = [H1, H2 | Pl].

merge(L1, L2, Merged):-
  length(L1, C1),
  length(L2, C2),
  (C2 < C1 ->
    merge_i(L1, L2, Merged);
    merge_i(L2, L1, Merged)).


