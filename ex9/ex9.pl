t(t(nil, b, nil), a, t(t(nil, d, nil), c, nil)).

in(X, t(_, X, _)).
in(X, t(Left, _, _)):-
  in(X, Left).
in(X, t(_, _, Right)):-
  in(X, Right).

count(nil, 0).
count(t(Left, _, Right), Count):-
  count(Left, LeftC),
  count(Right, RightC),
  Count is LeftC + RightC + 1.

depth(nil, 0).
depth(t(Left, _, Right), Count):-
  depth(Left, LeftD),
  depth(Right, RightD),
  Count is max(LeftD, RightD) + 1.

linearize(T, Result):-
  linearize(T, [], Result).
linearize(nil, Acc, Acc).
linearize(t(Left, Element, Right), Acc, Result):-
  linearize(Right, Acc, RightLin),
  linearize(Left, [Element|RightLin], Result).

vertex(t(_, _, _)).

close_tree(nil):-
  !.
close_tree(t(Left, _, Right)):-
  close_tree(Left),
  close_tree(Right).

occur(X, [X | _]).
occur(X, [E | Tail]):-
  E < X,
  occur(X, Tail).

occurthree(X, [_ , _, Pivot | Tail]):-
  Pivot < X,
  !,
  occurthree(X, Tail).
occurthree(X, [E1, E2, Pivot | _]):-
  Pivot >= X,
  !,
  member(X, [E1, E2, Pivot]).
occurthree(X, L):-
  member(X, L).

sorted(Tree):-
  sorted(Tree, _, _).
sorted(t(nil, E, nil), E, E).
sorted(t(Left, E, nil), Low, E):-
  sorted(Left, Low, High),
  High < E.
sorted(t(nil, E, Right), E, High):-
  sorted(Right, Low, High),
  E < Low.
sorted(t(Left, E, Right), Low, High):-
  sorted(Left, Low, LeftHigh),
  LeftHigh < E,
  sorted(Right, RightLow, High),
  E < RightLow.

balanced(T):-
  balanced(T, _).
balanced(nil, 0).
balanced(t(Left, _, Right), Depth):-
  balanced(Left, LeftDepth),
  balanced(Right, RightDepth),
  abs(LeftDepth - RightDepth) =< 1,
  Depth is max(LeftDepth, RightDepth) + 1.

take(List, 0, [], List).
take([H| ListT], N, [H| TakeT], Remainder):-
  NewN is N - 1,
  take(ListT, NewN, TakeT, Remainder).

split(List, Left, Pivot, Right):-
  length(List, Length),
  Half is Length // 2,
  take(List, Half, Left, [Pivot| Right]).

list_to_balanced([], nil).
list_to_balanced([E], t(nil, E, nil)).
list_to_balanced(List, t(Left, Pivot, Right)):-
  split(List, LeftList, Pivot, RightList),
  list_to_balanced(LeftList, Left),
  list_to_balanced(RightList, Right).







