list_length([], 0).
list_length([_ | T], L):-
  list_length(T, L1),
  L is L1 + 1.

list_length_2(List, Length):-
  list_length_2(List, 0, Length).

list_length_2([], L, L).
list_length_2([_| T], Accumulator, Length):-
  NewAcc is Accumulator + 1,
  list_length_2(T, NewAcc, Length).


quicksort([], []).
quicksort([X| Tail], Sorted):-
  split(X, Tail, Small, Big),
  quicksort(Small, SortedSmall),
  quicksort(Big, SortedBig),
  append(SortedSmall, [X| SortedBig], Sorted).

quicksort2(List, SortedList):-
  quicksort2(List, [], SortedList).
quicksort2([], Suffix, Suffix).
quicksort2([X | Tail], Suffix, Sorted):-
  split(X, Tail, Small, Big),
  quicksort2(Big, Suffix, SortedBig),
  quicksort2(Small, [X | SortedBig], Sorted).

split(_, [], [], []).
split(X, [Y| Tail], [Y| Small], Big):-
  X > Y, !,
  split(X, Tail, Small, Big).
split(X, [Y | Tail], Small, [Y| Big]):-
  split(X, Tail, Small, Big).

fib(1, 1).
fib(2, 1).
fib(N, S):-
  Prev is N - 1,
  fib(Prev, PrevS),
  PrevPrev is N - 2,
  fib(PrevPrev, PrevPrevS),
  S is PrevS + PrevPrevS.


fib_acc(N, S):-
  fib_acc(N, 0, S).
fib_acc(1, Acc, R):-
  R is Acc + 1.
fib_acc(2, Acc, R):-
  R is Acc + 1.
fib_acc(N, Acc, S):-
  PrevN is N - 1,
  fib_acc(PrevN, Acc, PrevS),
  PrevPrevN is N - 2,
  fib_acc(PrevPrevN, PrevS, S).

get_doubles(L, Duplicates):-
  get_doubles(L, [], [], Duplicates).
get_doubles([], _, Duplicates, Duplicates).
get_doubles([X| Tail], Singles, Duplicates, Result):-
  (memberRemove(X, Duplicates, _) ->
    get_doubles(Tail, Singles, Duplicates, Result);
    (memberRemove(X, Singles, SinglesWithoutX) ->
      get_doubles(Tail, SinglesWithoutX, [X| Duplicates], Result);
      get_doubles(Tail, [X | Singles], Duplicates, Result))).


memberRemove(X, [X | Tail], Tail).
memberRemove(X, [Y | Tail], [Y | Rest]):-
  memberRemove(X, Tail, Rest).

arc(From, To, Graph):-
  member(From/To, Graph).

connected(From, To, Graph):-
  arc(From, To, Graph).
connected(From, To, Graph):-
  arc(From, Intermediate, Graph),
  connected(Intermediate, To, Graph).

find_path(X, Y, Graph, [X/Y]):-
  member(X/Y, Graph).

find_path(From, To, Graph, [From/Intermediate | Rest]):-
  arc(From, Intermediate, Graph),
  find_path(Intermediate, To, Graph, Rest).

find_path_no_loop(X, Y, Graph, Path):-
  find_path_no_loop(X, Y, Graph, [], Path).

find_path_no_loop(X, Y, Graph, _, [X/Y]):-
  member(X/Y, Graph).

find_path_no_loop(From, To, Graph, Visited, [From/Intermediate | Rest]):-
  arc(From, Intermediate, Graph),
  \+member(Intermediate, Visited),
  find_path_no_loop(Intermediate, To, Graph, [From | Visited], Rest).

find_path_undirected(X, Y, Graph, Path):-
  find_path_undirected(X, Y, Graph, [], Path).

find_path_undirected(X, Y, Graph, _, [X/Y]):-
  arc_undirected(X, Y, Graph).

find_path_undirected(From, To, Graph, Visited, [From/Intermediate | Rest]):-
  arc_undirected(From, Intermediate, Graph),
  \+member(Intermediate, Visited),
  find_path_undirected(Intermediate, To, Graph, [From | Visited], Rest).

arc_undirected(From, To, Graph):-
  member(From/To, Graph).

arc_undirected(From, To, Graph):-
  member(To/From, Graph).
