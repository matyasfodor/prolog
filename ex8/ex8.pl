fill_table([], []).
fill_table([H | T], List):-
  between(1, 9, H),
  fill_table(T, SubList),
  List = [H | SubList].

no_duplicates([]).
no_duplicates([H | T]):-
  \+member(H, T),
  no_duplicates(T).


drop(List, 0, List).
drop([_ | T], N, Rest):-
  N > 0,
  NewN is N - 1,
  drop(T, NewN, Rest).

take(_, 0, []).
take([H | T], N, [H | Rest]):-
  N > 0,
  NewN is N - 1,
  take(T, NewN, Rest).

sub_list(List, FirstElement, N, Result):-
  drop(List, FirstElement, IntermedList),
  take(IntermedList, N, Result).


row_cheks(Table):-
  row_cheks(Table, 9).
row_cheks(Table, N):-
  OneSmaller is N - 1,
  findall(Mul, (
    between(0, OneSmaller, Mul),
    Offset is N * Mul,
    sub_list(Table, Offset, N, SubList),
    \+no_duplicates(SubList)
  ), Muls),
  (\+length(Muls, 0) -> 
    (
      %write('Row check failed in rows: '),
      %write(Muls),
      %write('\n'),
      false); true).


list_decrease([], []).
list_decrease([H| T], [DecH| DecT ]):-
  H > 0,
  DecH is H - 1,
  list_decrease(T, DecT).

pick_at_indices([], _, []).
pick_at_indices([0 | IndexTail], [H| ListTail], [H | PickTail]):-
  list_decrease(IndexTail, DecreasedTail),
  pick_at_indices(DecreasedTail, ListTail, PickTail).
pick_at_indices([H | T], [_ | ListTail], Pick):-
  list_decrease([H | T], DecreasedIndexList),
  pick_at_indices(DecreasedIndexList, ListTail, Pick).

column_checks(Table):-
  column_checks(Table, 9).
column_checks(Table, N):-
  OneSmaller is N - 1,
  findall(Column, (
    between(0, OneSmaller, Column),
    findall(Index, (
      between(0, OneSmaller, Row),
      Index is Column + Row * N),
    Indices),
    pick_at_indices(Indices, Table, SubList),
    \+no_duplicates(SubList)
  ), Columns),
  (\+length(Columns, 0) -> 
    (
      %write('Row check failed in rows: '),
      %write(Columns),
      %write('\n'),
      false); true).

block_indices(BRow, BColunm, Indices):-
  block_indices(BRow, BColunm, 3, 9, Indices).
block_indices(BRow, BColunm, N, RowLength, Indices):-
  BlockRow is BRow * N,
  BlockColumn is BColunm * N,
  findall(RowIndices, (
    StopRow is BlockRow + N - 1,
    between(BlockRow, StopRow, Row),
    findall(Index, (
      StopColumn is BlockColumn + N - 1,
      between(BlockColumn, StopColumn, Column),
      Index is Row * RowLength + Column
      ), RowIndices)
    ), ColumnIndices),
  flatten(ColumnIndices, Indices).

block_checks(Table):-
  block_checks(Table, 3).
block_checks(Table, N):-
  OneSmaller is N - 1,
  findall(Errors, (
    between(0, OneSmaller, Row),
    findall(Row/Column, (
      between(0, OneSmaller, Column),
      block_indices(Row, Column, Indices),
      pick_at_indices(Indices, Table, SubList),
      \+no_duplicates(SubList)
      ), Errors)
    ), CumErrors),
  flatten(CumErrors, AllErrors),
  (\+length(AllErrors, 0) -> 
    (
      %write('Block check failed in blocks: '),
      %write(AllErrors),
      %write('\n'),
      false); true).

sudoku(Table, FilledTable):-
  fill_table(Table, FilledTable),
  row_cheks(FilledTable),
  column_checks(FilledTable),
  block_checks(FilledTable).






