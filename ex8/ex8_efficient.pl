%% sequence_check helpers

sum(List, Result):-
  sum(List, 0, Result).
sum([], Result, Result).
sum([H | T], Acc, Result):-
  NewAcc is H + Acc,
  sum(T, NewAcc, Result).

no_duplicates([]).
no_duplicates([H | T]):-
  \+member(H, T),
  no_duplicates(T).

%% sequence_check helpers end

% Check if the given sequence contains each number only once.
% Should be augmented with sum for full sequences (eg sum of numbers 1-9 is 45, for 1-4 is 16 etc.)
sequence_check(List):-
  %sum(List, 45),
  no_duplicates(List).

%% helpers for getting and setting values
list_decrease([], []).
list_decrease([H| T], [DecH| DecT ]):-
  H > 0,
  DecH is H - 1,
  list_decrease(T, DecT).

pick_at_indices(_, [], []).
pick_at_indices([H| ListTail], [0 | IndexTail], [H | PickTail]):-
  list_decrease(IndexTail, DecreasedTail),
  pick_at_indices(ListTail, DecreasedTail, PickTail).
pick_at_indices([_ | ListTail], [H | T], Pick):-
  list_decrease([H | T], DecreasedIndexList),
  pick_at_indices(ListTail, DecreasedIndexList, Pick).

set_at_indices(R, [], _, R).
set_at_indices([_| OriginalTail], [0 | IndexTail], [H | ListTail], [H| ResultTail]):-
  list_decrease(IndexTail, DecreasedTail),
  set_at_indices(OriginalTail, DecreasedTail, ListTail, ResultTail).
set_at_indices([OriginalH| OriginalTail], [H | T], List, [OriginalH| ResultTail]):-
  list_decrease([H | T], DecreasedIndexList),
  set_at_indices(OriginalTail, DecreasedIndexList, List, ResultTail).

%% helpers for getting and setting values end

%% more specialized predicates for getting/setting row, columns, blocks
get_row_indices(Row, N, Result):-
  FirstIndex is Row * N,
  LastIndex is FirstIndex + N - 1,
  findall(Index, between(FirstIndex, LastIndex, Index), Result).

get_row(Table, Row, Result):-
  get_row(Table, Row, 9, Result).
get_row(Table, Row, N, Result):-
  get_row_indices(Row, N, Indices),
  pick_at_indices(Table, Indices, Result).

set_row(Table, Row, List, Result):-
  set_row(Table, Row, 9, List, Result).
set_row(Table, Row, N, List, Result):-
  get_row_indices(Row, N, Indices),
  set_at_indices(Table, Indices, List, Result).

get_column_indices(Column, Length, Result):-
  OneLess is Length - 1,
  findall(Index, (
    between(0, OneLess, Row),
    Index is Row * Length + Column
    ), Result).

get_column(Table, Column, Result):-
  get_column(Table, Column, 9, Result).
get_column(Table, Column, N, Result):-
  get_column_indices(Column, N, Indices),
  pick_at_indices(Table, Indices, Result).

set_column(Table, Column, List, Result):-
  set_row(Table, Column, 9, List, Result).
set_column(Table, Column, N, List, Result):-
  get_column_indices(Column, N, Indices),
  set_at_indices(Table, Indices, List, Result).

get_block_indices(BRow, BColunm, Indices):-
  get_block_indices(BRow, BColunm, 3, 9, Indices).
get_block_indices(BRow, BColunm, N, RowLength, Indices):-
  BlockRow is BRow * N,
  BlockColumn is BColunm * N,
  findall(RowIndices, (
    StopRow is BlockRow + N - 1,
    between(BlockRow, StopRow, Row),
    findall(Index, (
      StopColumn is BlockColumn + N - 1,
      between(BlockColumn, StopColumn, Column),
      Index is Row * RowLength + Column
      ), RowIndices)
    ), ColumnIndices),
  flatten(ColumnIndices, Indices).


get_block(Table, Column, Row, Result):-
  get_block(Table, Column, Row, 3, Result).
get_block(Table, Column, Row, N, Result):-
  NSquared is N * N,
  get_block_indices(Column, Row, N, NSquared, Indices),
  pick_at_indices(Table, Indices, Result).

%% more specialized predicates for getting/setting row, columns, blocks end

% Fill: Produces values for the given sequence
% this could be smarter by only generating permutations - a bit difficult with partially filled variables
fill([], _, []).
fill([H | T], N, List):-
  between(1, N, H),
  fill(T, N, SubList),
  List = [H | SubList].

% higher level functions that get values from already partially filled in rows/columns, fill them
% check the new sequence and if it passes they set them.
fill_row(Table, RowNumber, FullSize, Result):-
  get_row(Table, RowNumber, FullSize, Row),
  fill(Row, FullSize, FilledRow),
  sequence_check(FilledRow),
  set_row(Table, RowNumber, FullSize, FilledRow, Result).

fill_column(Table, ColumnNumber, FullSize, Result):-
  get_column(Table, ColumnNumber, FullSize, Column),
  fill(Column, FullSize, FilledColumn),
  sequence_check(FilledColumn),
  set_column(Table, ColumnNumber, FullSize, FilledColumn, Result).

% higher level functions end

%% Table print helpers
drop(List, 0, List).
drop([_ | T], N, Rest):-
  N > 0,
  NewN is N - 1,
  drop(T, NewN, Rest).

take(_, 0, []).
take([H | T], N, [H | Rest]):-
  N > 0,
  NewN is N - 1,
  take(T, NewN, Rest).

sub_list(List, FirstElement, N, Result):-
  drop(List, FirstElement, IntermedList),
  take(IntermedList, N, Result).
%% Table print helpers end

%% table_print: Visualize sudoku table
table_print(Table, N):-
  OneLess is N - 1,
  write('\nTable:\n'),
  findall(_, (
      between(0, OneLess, Row),
      StartIndex is Row * N,
      sub_list(Table, StartIndex, N, SubList),
      write(SubList),
      write('\n')
    ), _),
  write('\n').


% The algorithm proceeds from the rightmost column and bottom most row, recursively aproaching the top left corner.
% When a layer of blocks (sqrt(N) by sqrt(n) squares) is done, it does check for their consistency.
% Generator case. Just provide the row/column length and it spits out the solutions
sudoku(Size):-
  Squared is Size * Size,
  length(Table, Squared),
  findall(a, (
      sudoku(Table, Size, Size, FilledTable),
      table_print(FilledTable, Size)
    ), All),
  length(All, AllLength),
  write('All Length: '/AllLength).
% Solver for  9x9
sudoku(Table, FilledTable):-
  sudoku(Table, 9, 9, FilledTable).
% Base case for recursion
sudoku(Table, _, 0, Table).
sudoku(Table, FullSize, N, FilledTable):-
  OneLess is N - 1,
  BlockSize is round(sqrt(FullSize)),
  % Try to fill the N-1 th row first, and check for duplicates
  fill_row(Table, OneLess, FullSize, RowFilledTable),
  % Try to fill the N-1 th column next, and check for duplicates
  fill_column(RowFilledTable, OneLess, FullSize, RowColumnFilledTable),
  % If block are completed, check for their consistency
  Remainder is OneLess mod BlockSize,
  (Remainder = 0 -> 
    (Sides is OneLess / BlockSize,
    OneLessSides is Sides - 1,
    get_block(RowColumnFilledTable, Sides, Sides, BlockSize, CornerSeq),
    sequence_check(CornerSeq),
    findall(
      Row/Sides,
      (
        between(0, OneLessSides, Row),
        get_block(RowColumnFilledTable, Sides, Row, BlockSize, Seq),
        \+sequence_check(Seq)
      ),
      VerticalErrors),
    length(VerticalErrors, 0),
    findall(
      Sides/Column,
      (
        between(0, OneLessSides, Column),
        get_block(RowColumnFilledTable, Column, Sides, BlockSize, Seq),
        \+sequence_check(Seq)
      ),
      HorizontalErrors),
    length(HorizontalErrors, 0));true),
  NewN is N - 1,
  sudoku(RowColumnFilledTable, FullSize, NewN, FilledTable).




