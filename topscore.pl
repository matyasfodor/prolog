score(danny, fai, 20).
score(danny, plpm, 15).
score(toon, fai, 18).
score(toon, plpm, 14).

%% Queries:

%% topscore_terms([score(danny, fai, 20),
%%   score(danny, plpm, 15),
%%   score(toon, fai, 18),
%%   score(toon, plpm, 14)], TopScore).

%% topscore_facts(TopScore).

topscore_terms(List, TopScore):-
  topscore_terms(List, -1, TopScore).

topscore_terms([], TopScore, TopScore).
topscore_terms([score(_, _, Score) | Tail], PrevTopScore, TopScore):-
  Score > PrevTopScore,
  topscore_terms(Tail, Score, TopScore).
topscore_terms([score(_, _, Score) | Tail], PrevTopScore, TopScore):-
  Score =< PrevTopScore,
  topscore_terms(Tail, PrevTopScore, TopScore).


topscore_facts(TopScore):-
  findall(
    score(Name, Course, Score), 
    score(Name, Course, Score),
    List),
  topscore_terms(List, TopScore).
