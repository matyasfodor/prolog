append2(L1-H1, H1-H2, L1-H2).

flatten2(List, Flatlist):-
  flatten_diff(List, Flatlist-[]).
flatten_diff([], L-L).
flatten_diff(X, [X| L]-L):-
  atomic(X),
  X \== [].
flatten_diff([H| T], H1-L):-
  flatten_diff(H, H1-E1),
  flatten_diff(T, E1-L).


linearize(Tree, Linearized):-
  lin(Tree, Linearized-[]).
lin(nil, L-L).
lin(t(Left,Root,Right), H1-E3):-
  lin(Left, H1-[Root| E2]),
  lin(Right, E2-E3).
