decimal(2) --> [twenty].
decimal(3) --> [thirty].
decimal(4) --> [fourty].
decimal(5) --> [fifty].
decimal(6) --> [sixty].
decimal(7) --> [seventy].
decimal(8) --> [eighty].
decimal(9) --> [ninety].

%% This only works in one direction!
%% Should rather use N = N1 + N2. and generate all posible
%% solutions
n(1) --> [one].
n(2) --> [two].
n(3) --> [three].
n(4) --> [four].
n(5) --> [five].
n(6) --> [six].
n(7) --> [seven].
n(8) --> [eight].
n(9) --> [nine].
n(10) --> [ten].
n(11) --> [eleven].
n(12) --> [twelve].
n(13) --> [thirteen].
n(14) --> [fourteen].
n(15) --> [fifteen].
n(16) --> [sixteen].
n(17) --> [seventeen].
n(18) --> [eighteen].
n(19) --> [nineteen].
n(X) -->
  {
    X < 100,
    X > 19,
    Decimal is X mod 10,
    Rest is X // 10
  },
  decimal(Rest),
  n(Decimal).

