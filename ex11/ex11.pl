fac(N,F) :- { N = 0, F = 1 }.
fac(N,F) :-
  { 1 =< N, N1 = N - 1, F = N * F1,
    N =< F}, % redundant constraint
  fac(N1,F1).

% balance(P, T, I, MP, B)
balance(P, 0, _, _, P).
balance(P, T, I, MP, B):-
  T1 is T - 1,
  balance(P, T1, I, MP, B1),
  B is B1 * (1 + I) - MP.

balance2(P, T, _, _, B):-
  {
    T = 0,
    B = P
  }.

balance2(P, T, I, MP, B):-
  {
    T > 0,
    T1 = T - 1,
    B = B1 * (1.0 + I) - MP
  },
  balance2(P, T1, I, MP, B1).
