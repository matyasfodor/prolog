safe(R):-
  R = [N1, N2, N3, N4, N5, N6, N7, N8, N9],
  [N1, N2, N3, N4, N5, N6, N7, N8, N9] ins 1..9,
  N1 #\= 1,
  N2 #\= 2,
  N3 #\= 3,
  N4 #\= 4,
  N5 #\= 5,
  N6 #\= 6,
  N7 #\= 7,
  N8 #\= 8,
  N9 #\= 9,
  N7 #= abs(N6- N4),
  N1 * N2 * N3 #= N8 + N9,
  N2 + N3 + N6 #< N8,
  N9 #< N8,
  all_different([N1, N2, N3, N4, N5, N6, N7, N8, N9]),
  labeling([], [N1, N2, N3, N4, N5, N6, N7, N8, N9]).

dinner(M, N, [], [], Arrangement):-
  Length #= M * N,
  length(Arrangement, Length),
  Arrangement ins 1..Length,
  all_different(Arrangement).

dinner(M, N, [like(I, J) | LikesTail], [], Arrangement).
