book(1,'The art of Prolog',400).
book(23,'The mistery of Strawberries',42).
person('Statler').
person('Waldorf').
author('Statler',1).
author('Waldorf',23).
hates('Statler',1).
owns('Waldorf',23).

queryISBN(ISBN) :- book(ISBN, _, _).

hatedBooks(Name):- book(ISBN, Name, _), author(Person, ISBN), hates(Person, ISBN).

nicePrint():-
  book(ISBN, BookName, _),
  author(Person, ISBN),
  format('the book ~w is written by author ~w', [BookName, Person]).

proud_author(Person):-
  book(ISBN, _, _),
  owns(Person, ISBN).

konyv(ISBN, Name, Pages):- book(ISBN, Name, Pages).
szemely(Name):- person(Name).
szerzo(Name, ISBN):- author(Name, ISBN).
utal(Name, ISBN):- hates(Name, ISBN).
birtokol(Name, ISBN):- owns(Name, ISBN).
