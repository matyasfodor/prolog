color_rectangle(R, Solution):-
  color_rectangle(R, 1, 1, [], Solution).



%% Base case
%% Ordering can be done here
color_rectangle([[]], _, _, CurrentSolution, Solution):-
  sort(CurrentSolution, Solution).
color_rectangle([[]| T], _, Y, CurrentSolution, Solution):-
  NewY is Y + 1,
  color_rectangle(T, 1, NewY, CurrentSolution, Solution).
color_rectangle([[H| T1]| T2], X, Y, CurrentSolution, Solution):-
  unknown_or_name(H, '.'),
  !,
  NewX is X + 1,
  color_rectangle([T1| T2], NewX, Y, CurrentSolution, Solution).
color_rectangle([[H| T1]| T2], X, Y, CurrentSolution, Solution):-
  update_solution(CurrentSolution, H, X, Y, NewSolution),
  NewX is X + 1,
  color_rectangle([T1| T2], NewX, Y, NewSolution, Solution).

%% It checks if there's an entry for name already. If yes, then it updates it if necessary, otherwise it adds it.
%% Base-case - not found
update_solution([], Name, X, Y, [Name-rhk(X, X, Y, Y)]).
%% Generic case - found, do not recurse
update_solution([Name-rhk(SX, EX, SY, EY)| T], Name, X, Y, [Name-rhk(NSX, NEX, NSY, NEY)| T]):-
  !,
  NSX is min(SX, X),
  NEX is max(EX, X),
  NSY is min(SY, Y),
  NEY is max(EY, Y).
%% Generic case - not found, recurse
update_solution([H| T], Name, X, Y, [H| NewTail]):-
  update_solution(T, Name, X, Y, NewTail).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sign of unknown is '#'

unknown_or_name('#', _).
unknown_or_name(Name, Name).

%% Checks if within the rectangle all values are 'Name' or '#'
%% R = [
%%   [h, #],
%%   [#, h],
%% ], all_name_or_unknown(R, h).
%% returns true
%% R = [
%%   [h, #],
%%   [#, a],
%% ], all_name_or_unknown(R, h).
%% returns false
all_name_or_unknown([], _).
all_name_or_unknown([[]| T], Name):-
  all_name_or_unknown(T, Name).
all_name_or_unknown([[H| T1]| T2], Name):-
  unknown_or_name(H, Name),
  all_name_or_unknown([T1 | T2], Name).

%% Gets index from location
%% R = [
%%   [h, #],
%%   [#, h],
%% ], get_index(R, 2, 1, #).
get_index([[H| _]| _], 1, 1, H):-!.
get_index([[_| T1]| T2], X, 1, S):-
  !,
  NewX is X - 1,
  get_index([T1 | T2], NewX, 1, S).
get_index([_| T], X, Y, S):-
  !,
  NewY is Y - 1,
  get_index(T, X, NewY, S).

%% Sets index from location
%% R = [
%%   [h, #],
%%   [#, h],
%% ], get_index(R, 2, 1, s, NewR).
%% NewR = [
%%   [h, s],
%%   [#, h],
%% ]
set_index([[_| T1]| T2], 1, 1, Value, [[Value| T1]| T2]):-!.
set_index([[H| T1]| T2], X, 1, Value, [[H| RT1]| RT2]):-
  !,
  NewX is X - 1,
  set_index([T1 | T2], NewX, 1, Value, [RT1| RT2]).
set_index([H| T], X, Y, Value, [H| S]):-
  !,
  NewY is Y - 1,
  set_index(T, X, NewY, Value, S).

%% Operates on a list,
%% l_all_name_or_unknown([h, h, h, #], h).
%% true
l_all_name_or_unknown([], _).
l_all_name_or_unknown([H| T], Name):-
  unknown_or_name(H, Name),
  l_all_name_or_unknown(T, Name).

%% True if the rectangle bounded by the indices SX, EX, SY, EY
%% are all either Name or #
name_can_be_top(R, Name, SX, EX, SY, EY):-
  findall(S, (
    between(SX, EX, X),
    between(SY, EY, Y),
    get_index(R, X, Y, S)
  ), Elements),
  l_all_name_or_unknown(Elements, Name).

%% Filters Rects and returns the ones that can be on top
can_be_top(R, Rects, Solution):-
  can_be_top(R, Rects, [], Solution).
%% Ordering can be done here - if necessary
can_be_top(_, [], CurrentTopOnes, CurrentTopOnes).
can_be_top(R, [Name-rhk(SX, EX, SY, EY)| Rects], CurrentTopOnes, Solution):-
  !,
  name_can_be_top(R, Name, SX, EX, SY, EY),
  can_be_top(R, Rects, [Name-rhk(SX, EX, SY, EY)| CurrentTopOnes], Solution).
can_be_top(R, [_| Rects], CurrentTopOnes, Solution):-
    can_be_top(R, Rects, CurrentTopOnes, Solution).

%% Replaces inidces in R with #
replace_with_unknown(R, _, [], R).
replace_with_unknown(R, Name, [X-Y| T], Solution):-
  set_index(R, X, Y, '#', NewR),
  replace_with_unknown(NewR, Name, T, Solution).

%% Replaces indices od the obtained rectangles with #
l_replace_with_unknown(R, [], R).
l_replace_with_unknown(R, [Name-rhk(SX, EX, SY, EY)| T], Solution):-
  findall(X-Y, (
    between(SX, EX, X),
    between(SY, EY, Y)
  ), Elements),
  replace_with_unknown(R, Name, Elements, NewR),
  l_replace_with_unknown(NewR, T, Solution).

%% Main function, runs until th whole Rectangl contains only empty or unknown field
sequence(R, Solution):-
  color_rectangle(R, Rects),
  sequence(R, Rects, [], Solution).
%% If all chars are empty ('.' or unknown '#') then recurse
sequence(R, _, Solution, Solution):-
  !,
  all_name_or_unknown(R, '.').
sequence(R, Rects, CurrentSolution, Solution):-
  can_be_top(R, Rects, CanBeTop),
  l_replace_with_unknown(R, CanBeTop, NewR),
  append(CurrentSolution, CanBeTop, NewCurrentSolution),
  sequence(NewR, Rects, NewCurrentSolution, Solution).


