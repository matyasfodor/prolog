% state(S, B)
% inital state: solve(state(0, 0), R).

is_solution(state(8, _)).
is_solution(state(_, 8)).

% try_action(state(S, B), state(S, B)).

% Fill the jug from river
try_action(state(_, B), state(15, B)).
try_action(state(S, _), state(S, 16)).

% Empty the jug completely
try_action(state(_, B), state(0, B)).
try_action(state(S, _), state(S, 0)).

% Empty one jug to the other
try_action(state(S, B), state(0, NB)):-
  NB is S + B,
  NB =< 16.
try_action(state(S, B), state(NS, 16)):-
  NS is S + B - 16,
  NS =< 15,
  NS >= 0.
try_action(state(S, B), state(NS, 0)):-
  NS is S + B,
  NS =< 15.
try_action(state(S, B), state(15, NB)):-
  NB is B + S - 15,
  NB =< 16,
  NB >= 0.

no_loop(NewState, AccTrace):-
  \+member(NewState, AccTrace).

solve(InitialState, Trace) :-
  search(InitialState, [InitialState], Trace).

search(CurrentState, AccTrace, Trace):-
  is_solution(CurrentState),
  Trace = AccTrace.

search(CurrentState, AccTrace, Trace):-
  try_action(CurrentState, NewState),
  %% validate_state(NewState),
  no_loop(NewState, AccTrace),
  write(NewState),nl,
  search(NewState,[NewState | AccTrace], Trace).
