merge([], T, T).
merge(T, [], T).
merge([H1| T1], [H2| T2], [H1| L]):-
  integer(H1),
  integer(H2),
  H1 =< H2,
  merge(T1, [H2| T2], L).
merge([H1| T1], [H2| T2], [H2| L]):-
  integer(H1),
  integer(H2),
  H1 > H2,
  merge([H1| T1], T2, L).

merge([move(M1, N11, N12)| T1], [move(M2, N21, N22)| T2], [move(M1, N11, N12) | L]):-
  N11 > N21,
  merge(T1, [move(M2, N21, N22)| T2], L).
merge([move(M1, N11, N12)| T1], [move(M2, N21, N22)| T2], [move(M1, N11, N12) | L]):-
  N11 = N21,
  N12 =< N22,
  merge(T1, [move(M2, N21, N22)| T2], L).

merge([move(M1, N11, N12)| T1], [move(M2, N21, N22)| T2], [move(M2, N21, N22) | L]):-
  N11 < N21,
  merge([move(M1, N11, N12)| T1], T2, L).
merge([move(M1, N11, N12)| T1], [move(M2, N21, N22)| T2], [move(M2, N21, N22) | L]):-
  N11 = N21,
  N12 > N22,
  merge([move(M1, N11, N12)| T1], T2, L).

splitl([H| T], [H| Left], Rigth):-
  splitr(T, Left, Rigth).
splitl([], [], []).

splitr([H| T], Left, [H | Rigth]):-
  splitl(T, Left, Rigth).
splitr([], [], []).

split(List, Left, Rigth):-
  splitl(List, Left, Rigth).

mergesort([], []).
mergesort([A], [A]):-
  !.
mergesort(List, Sorted):-
  split(List, L, R),
  mergesort(L, LSorted),
  mergesort(R, RSorted),
  merge(LSorted, RSorted, Sorted).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Missionaries vs Cannibals

%% InitialState
%% state(3, 3, 0, 0, 0, 0, left, arrived).

mc_is_solution(t(0, 0, _)).

gen_numbers(Top1, Top2, N1, N2):-
  Ceil1 is min(Top1, 2),
  between(0, Ceil1, N1),
  Inv is 2 - N1,
  Ceil2 is min(Top2, Inv),
  between(0, Ceil2, N2),
  N1 + N2 > 0.

%% left -> moving
mc_try_action(t(M, C, left), t(NM, NC, right)):-
  gen_numbers(M, C, N1, N2),
  NM is M - N1,
  NC is C - N2.

mc_try_action(t(M, C, right), t(NM, NC, left)):-
  gen_numbers(3 - M, 3 - C, N1, N2),
  NM is M + N1,
  NC is C + N2.

validate_left(0, _).
validate_left(M, C):-
  M >= C.

validate_right(3, _).
validate_right(M, C):-
  M =< C.

mc_validate_state(t(M, C, _)):-
  validate_left(M, C),
  validate_right(M, C).

mc_no_loop(NewState, AccTrace):-
  \+member(NewState, AccTrace).

disp(t(M, C, BoatState)):-
  write(M),write('\t'),write(C),write('\t'),write(BoatState),nl.

solve(InitialState, Trace) :-
        search(InitialState, [InitialState], Trace).

search(CurrentState, AccTrace, Trace):-
        mc_is_solution(CurrentState),
        Trace=AccTrace.

search(CurrentState, AccTrace, Trace):-
        mc_try_action(CurrentState, NewState),
        mc_validate_state(NewState),
        mc_no_loop(NewState, AccTrace),
        %% disp(NewState),
        search(NewState, [NewState| AccTrace],Trace).
